package com.ctl.it.qa.sample.tests;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import java.io.ObjectInputStream.GetField;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.ctl.it.qa.staf.Environment;
import com.ctl.it.qa.staf.Page;
import com.ctl.it.qa.staf.RallyTools;
import com.ctl.it.qa.staf.STAFEnvironment;
import com.ctl.it.qa.staf.Steps;
import com.ctl.it.qa.staf.TestEnvironment;
import com.ctl.it.qa.staf.xml.reader.IntContainerField;

import cucumber.api.CucumberOptions;

@TestEnvironment(Environment.TEST1) // Test Environment on which execution is to happen is provided
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/", tags = { "@newtour" })
public class CukesTest {
	@BeforeClass
	public static void setEnvironment() {

		STAFEnvironment.registerEnvironment(CukesTest.class);
		Steps.initialize("sample.xml");// Data input file name (present in SampleTools/src/test/resources) is provided
		RallyTools.initiateRallyLogin();// Method to initialize CaaC login
	}

	@AfterClass
	public static void clearEnvironment() {

		RallyTools.closeRallyAPI();// Method to kill CaaC api process
	}

	/*
	 * NOTE: CaaC test set/ test case result will be updates only when execution
	 * happens from Jenkins
	 */
}