package com.ctl.it.qa.sample.tests.steps.newtour;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import com.ctl.it.qa.sample.tools.pages.newtour.RegistrationfinalPage;
import com.ctl.it.qa.sample.tools.steps.newtour.NewtourSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NewtourStepDefinition {

	@Steps
	NewtourSteps newtourSteps;

	@Given("^I am in newtour application registration form$")
	public void navigate_to_NewTour_Registration_application() {
		newtourSteps.login_newtour();
	}

	@When("^I submitted the registration form$")
	public void fill_and_submit_registration_form() {
		newtourSteps.newtour_registration_();
	}

	@Then("^Successfull messese should display$")
	public void registration_successfull_validation() {
		newtourSteps.success_validation();
	}

}
